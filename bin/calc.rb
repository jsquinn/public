require 'irb/completion'
require 'pp'
require 'mathn'
include Math

IRB.conf[:AUTO_INDENT] = true
IRB.conf[:USE_READLINE] = true

def hex(x)
	x.to_i.to_s(16)
end
