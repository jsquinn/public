Company

Physical Assets
	- product stock
	- components
	- warehouses
	- buildings
	- vehicles
	- machines
	- computers
	- furniture
	
Intellectual property
	- copyrights
	- software

Systems
	- configuration, fault 

Employees
	- technical experience
	- understanding of business
	- grown into their responsibilities/roles in the company
	- organisation structure
	- behaviour

Customers
	- goodwill
	- recognition (of what company provides, quality)
	- details

Suppliers
	- contacts
	- trust

	
Ultimately the share price of a company is expected to reflect the potential profitability (~dividends) other the company.
While physical assets will be included in taxable asset growth but other parts of the company not although the employee cost to put them in place is charged.
Gains in the share price may have little to do with the costs incurred by the company to grow and therefore these gains are not covered by income taxes.
Potentially competition in the market will reduce a companies value to the true cost of setup...?