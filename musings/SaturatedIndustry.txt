Limits of production:

Humans only need enough each, they cannot individually consume infinite resources.
They only need so much to eat, drink, breath, wear, hump etc.

Does this mean there is a limit to the maximum required energy per human?
The more efficient this energy consumption is the more human effort is presumably required.

If we can maintain a comfortable existance with only a quarter time job per human how can we make this work?
Some one who works a full time job will put 3 people out of work and likely will be more efficient than all 4.
In my experience the more people working on a project the greater the time each spend communicating so how inefficient will it be?

With capitalism is this is what will happen?
But having 5-6 people work a full time job at 25% each (assuming extra 50% due to inefficiency) gives a lot of redundancy.

Saturated developed world requirements
 food, clothing, entertainment, basic medical
 communication
 
Unsaturated
  advanced medical, 
  storage, conveninent transport
  knowledge/science

Saturated industry
  - no new customers to find even if price drops?
  - no growth, sales are mostly replacement demand
  - only considering developed world
  - Products of higher quality than mainstream offerings are always in demand if they become cheaper 
      but they would superseed old options rather than being purchased in addition
  - there is still room for undercutting the competion
  - greater efficiencies result in employee redundancies
 
  Food (human)
  Clothing
  Personal Computers
  Phones and gadgets?
  full sized cars and motorcycles
  Medical diagnosis?
  firearms (legal restrictions)
  'minimal' housing? (desperation housing)
  simple tools
  Seeds?
  housing 'land' outside of cities for 


  
Unsaturated economies in the developed world
 - people would buy more if they were cheaper
 - room for growth if efficiencies are found
 - lower cost would encourage greater custom, expansion in economy and employment
 - only considering developed world

  Construction materials
  Building Insulation
  Underground/elevated buildings/storage
  Automated chores - washing, cleaning, groceries
  House and building maintenance (repainting, patching etc)
  Transportation Energy (gasoline)
  Energy - particularly safe nuclear/solar/wind
  Software, data entry and automation in business and even odd jobs
  Automated Medical diagnosis (e.g. MRI)
  Ammunition?
  Pollution reversal and recycling (Household rubbish, CO2)
  Water desalination purification, and irrigation
  Inventory reduction
  Goods Transportation
  Server computation, storage
  internet performance (latency + bandwidth)
  Business transport cost and time
  Holidays - flights, hotels, attractions
  Childcare (Organised and not organised)
  Elderly care?
  Animal feed, insecticides?
  Small cars? (elio)
  sports vehicles/racetracks
  (elevated/subway) Train systems/walkways/cycling areas
  Entertainment? - film, music
  Soil?
  housing 'land' inside of cities
  farm land

Unsaturated services
  construction labour
  



Low-skilled labour options

  Lollipop men
  cleaning
  garden care
  house maintenance?
  childcare
  elderly/disabled care
  Retail
  Food and catering
  transport
    - goods (trucking)
    - people (buses, taxis)
    - mail
  waste management
  inventory management
  social work?

Entertainment?
  - busking

  
  


