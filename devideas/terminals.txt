Terminals Dev Seminar
	Terminals 
		Teleprinters -> Mainframe computers
		Graphical terminals [VT100] <-> Mainframe computers
		serial/parallel communication
		ascii
			printable characters
			Control characters
			Backspace
			Modifiers
				Control, shift, Caps lock, num lock?, scroll lock?
			Escape sequences
				to linux:
					udlr, home, end, insert, delete
					F1-F12, Alt combinations
					terminal type, rows, colums
					mouse
					
				to screen:
					cursor location
					colours

		Key interpretation and character display
			printable characters (echo'd)
			Control characters
			New lines?
			
		Controller character handling -> Signals kernel
			Break
			stty
			Ctrl
			Ctrl+C, 
			newline, return feed
			backspace
			sys break
			pause
			
		Escape sequence handling
			readline
			libtermcap
			$TERM
			ls --color | cat -v
			
		tty sessions
			ps command
			signals
			
		Modern terminals:
			virtual: local keyboard, screen $TERM = linux
			serial 
			SSH $TERM = xterm 256 colour
				window resizing
			xterm
				utf8
				
		Consoles
			/dev/console
			/dev/tty
			/dev/ttyx
			/dev/ttySx
			... dynamic for SSH
		
		applications
			ps a
			getty
			login
			bash
			ping
			ls
			vim
			dmesg
			
	Get out of jail:
		Ctrl+Alt+F7 (graphical)
		Ctrl+C Ctrl+\ Ctrl+Q Ctrl+D
		killall -9 <cmd>
		vim: Ctrl+C Q :q!
		stty sane
		
	implications:
		keyboards (Escape/Ctrl) 
		character sets (ascii, utf8)
		unix
			- tty devices
			- home ~
			- signals
			- terminals
			- application piping
			- stdin, stdout, stderr
		C standard
			- null terminated ascii characters
			- stdin, stdout, stderr
			- lower case only?
		
	Further
		bash help
		bash shortcuts
		linux file system
		commands --help
		man pages
		git
		