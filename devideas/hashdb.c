#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <time.h>

// options
// populate db with rand
// read db to out
// check db with file
// load db with file
// show rand


struct record_t
{
	uint32_t key;
	uint32_t value;
};

struct recordlist_t
{
	struct record_t * records;
	size_t num;
};

// Key 0x12345678 -> "db/12/34/56"

static char DBHEXCHAR[] = { 'G', 'H', 'I', 'J',
				 	 'K', 'L', 'M', 'N',
					 'O', 'P', 'Q', 'R',
					 'S', 'T', 'U', 'V' };
typedef char recordpath_t[16];

static size_t DBRECORDSIZE   = 6;
static size_t DBRECORDMETA   = 0;
static size_t DBRECORDKEY    = 1;
static size_t DBRECORDVALUE0 = 2;
static size_t DBRECORDVALUE1 = 3;
static size_t DBRECORDVALUE2 = 4;
static size_t DBRECORDVALUE3 = 5;

static size_t DBPATHSUBDOFF[] = {2, 5, 8};	// index of '/'

void getPathFromKey(uint32_t key, recordpath_t path)
{
	path[0] = 'd';
	path[1] = 'b';
	path[2] = '/';
	path[3] = DBHEXCHAR[(key >> 28) & 0xf];
	path[4] = DBHEXCHAR[(key >> 24) & 0xf];
	path[5] = '/';
	path[6] = DBHEXCHAR[(key >> 20) & 0xf];
	path[7] = DBHEXCHAR[(key >> 16) & 0xf];
	path[8] = '/';
	path[9] = DBHEXCHAR[(key >> 12) & 0xf];
	path[10] = DBHEXCHAR[(key >> 8) & 0xf];
	path[11] = '\0';
}

uint8_t getSubKeyFromKey(uint32_t key)
{
	return key & 0xff;
}

void printRecords(struct recordlist_t *list, size_t num)
{
	recordpath_t path;

	for (size_t i = 0; i < list->num && i < num; i++)
	{
		getPathFromKey(list->records[i].key, path);
		printf("%08X %u %s\n", list->records[i].key, list->records[i].value, path);
	}
}

struct recordlist_t generateRecords(size_t num)
{
	struct recordlist_t list;
	list.num = 0;
	list.records = malloc(sizeof(struct record_t)*num);

	for (size_t i = 0; i < num ; i++)
	{
		static_assert(RAND_MAX > 1<<16);
		uint32_t key = rand()*rand();

		size_t j = 0;
		while (j < i)
		{
			if (list.records[j].key == key)
			{
				j = 0;
				key = rand()*rand();
				continue;
			}

			++j;
		}

		list.records[i].key = key;
		list.records[i].value = rand();
	}
	list.num = num;
	return list;
}

void regenerateRecords(struct recordlist_t list)
{
	for (size_t i = 0; i < list.num ; i++)
	{
		list.records[i].value = rand();
	}
}

void freeRecords(struct recordlist_t* list)
{
	free(list->records);
	list->records = NULL;
	list->num = 0;
}

int writeRecordReplace(struct record_t r, int fd)
{
	uint8_t buf[DBRECORDSIZE*256];
	int result = read(fd, buf, sizeof(buf));
	if (result < 0)
		return -errno;

	if (result < sizeof(buf))
		memset(buf + result, 0, sizeof(buf) - result);

	size_t offset = getSubKeyFromKey(r.key);
	uint8_t * poffset = buf + (DBRECORDSIZE*offset);
	if (poffset[DBRECORDMETA] == 1)
	{
		assert(poffset[DBRECORDKEY] == offset);
		uint32_t current = (poffset[DBRECORDVALUE0] << 24 | poffset[DBRECORDVALUE1] << 16
					| poffset[DBRECORDVALUE2] << 8| poffset[DBRECORDVALUE3]);

		if (r.value == current)
			return 1;
	}
	else
	{
		poffset[DBRECORDMETA] = 1;
		poffset[DBRECORDKEY] = offset;
	}

	poffset[DBRECORDVALUE0] = (r.value >> 24) & 0xff;
	poffset[DBRECORDVALUE1] = (r.value >> 16) & 0xff;
	poffset[DBRECORDVALUE2] = (r.value >> 8) & 0xff;
	poffset[DBRECORDVALUE3] = (r.value) & 0xff;

	int off = lseek(fd, 0, SEEK_SET);
	if (off != 0)
		return -errno;

	result = write(fd, buf, sizeof(buf));
	if (result < 0)
		return -errno;
	
	return 0;
}

int writeRecord(struct record_t r)
{
	int result;
	recordpath_t path;
	getPathFromKey(r.key, path);
	int fd = open(path, O_RDWR);
	if (fd < 0)
	{
		// create sub directories and create file
		// Key 0x12345678 -> "db/12/34/56"

		char * subpath = path;
		do
		{
			subpath = strchr(subpath, '/');
			if (!subpath)
				break;
			subpath[0] = '\0';
			int result = mkdir(path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
			if (result < 0 && errno != EEXIST)
				return -errno;
			subpath[0] = '/';
			subpath++;
		} while (1);

		fd = open(path, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
		if (fd < 0)
			return -errno;
	}
	result = writeRecordReplace(r, fd);
	close(fd);
	return result;
}

int readRecord(struct record_t *r)
{
	int result;
	recordpath_t path;
	getPathFromKey(r->key, path);
	int fd = open(path, O_RDONLY);
	if (fd < 0)
	{
		printf("could not open %s\n", path);
		return -errno;
	}

	uint8_t buf[DBRECORDSIZE*256];
	result = read(fd, buf, sizeof(buf));
	close(fd);
	if (result < 0)
	{
		printf("could not read %lu from %s\n", sizeof(buf), path);
		return -errno;
	}

	size_t offset = getSubKeyFromKey(r->key);

	if (result < DBRECORDSIZE*(offset+1))
	{
		printf("only %u bytes in %s\n", result, path);
		return -ENOENT;
	}

	uint8_t * poffset = buf + (DBRECORDSIZE*offset);
	if (poffset[DBRECORDMETA] == 1)
	{
		assert(poffset[DBRECORDKEY] == offset);
		r->value = poffset[DBRECORDVALUE0] << 24
			| poffset[DBRECORDVALUE1] << 16
			| poffset[DBRECORDVALUE2] << 8
			| poffset[DBRECORDVALUE3];
		return 0;
	}
	else
	{
		printf("meta not 1 for %u bytes in %s\n", r->key, path);
		return -ENOENT;
	}
}

int writeDeleteReplace(uint32_t key, int fd)
{
	uint8_t buf[DBRECORDSIZE*256];
	int result = read(fd, buf, sizeof(buf));
	if (result < 0)
		return -errno;

	if (result < sizeof(buf))
		memset(buf + result, 0, sizeof(buf) - result);

	size_t offset = getSubKeyFromKey(key);
	uint8_t * poffset = buf + (DBRECORDSIZE*offset);

	if (poffset[DBRECORDMETA] == 1)
	{
		assert(poffset[DBRECORDKEY] == offset);
	}
	else
	{
		return 1;
	}

	poffset[DBRECORDMETA] = 0;
	poffset[DBRECORDKEY] = 0;
	poffset[DBRECORDVALUE0] = 0;
	poffset[DBRECORDVALUE1] = 0;
	poffset[DBRECORDVALUE2] = 0;
	poffset[DBRECORDVALUE3] = 0;

	int off = lseek(fd, 0, SEEK_SET);
	if (off != 0)
		return -errno;

	result = write(fd, buf, sizeof(buf));
	if (result < 0)
		return -errno;
	
	return 0;
}

int deleteRecord(uint32_t key)
{
	int result;
	recordpath_t path;
	getPathFromKey(key, path);
	int fd = open(path, O_RDWR);
	if (fd < 0)
		return 0;
	result = writeDeleteReplace(key, fd);
	close(fd);
	return result;
}

int main(int argc, char *argv[])
{
	struct recordlist_t list = {};
	printf("Hello World\n");
	int dbfd = open("db", O_RDONLY);
	if (dbfd < 0)
	{
		printf("error: db directory doesn't exist\n");
		return -errno;
	}

	while (1)
	{
		printf("!>");
		char *line = NULL;
		size_t lineLen = 0;
		int result = getline(&line, &lineLen, stdin);
		if (result < 0)
			return 0;

		static const char * CMDEXIT = "exit";
		static const char * CMDGEN = "gen";
		static const char * CMDREGEN = "regen";
		static const char * CMDSHOW = "show";
		static const char * CMDREAD = "read";
		static const char * CMDWRITE = "write";
		static const char * CMDDEL = "del";
		if (strncmp(line, CMDEXIT, strlen(CMDEXIT)) == 0)
		{
			printf("Exiting\n");
			return 0;
		}
		else if (strncmp(line, CMDGEN, strlen(CMDGEN)) == 0)
		{
			char gen[16];
			int num = 0;
			result = sscanf(line, "%s %i", gen, &num);
			if (result != 2 || num < 0)
			{
				printf("Could not parse gen param [%i]\n", num);
			}
			else
			{
				freeRecords(&list);
				clock_t start = clock();
				list = generateRecords(num);
				clock_t time = clock() - start;
				printf("gen %i completed in %li\n", num, time);
			}
		}
		else if (strncmp(line, CMDREGEN, strlen(CMDREGEN)) == 0)
		{
			regenerateRecords(list);
		}
		else if (strncmp(line, CMDSHOW, strlen(CMDSHOW)) == 0)
		{
			char gen[16];
			int num = 0;
			result = sscanf(line, "%s %i", gen, &num);
			if (result != 2 || num < 0)
			{
				num = list.num;
			}
			printRecords(&list, num);
		}
		else if (strncmp(line, CMDREAD, strlen(CMDREAD)) == 0)
		{
			size_t succeeded = 0;
			clock_t start = clock();
			for (size_t i = 0; i < list.num; i++)
				if (readRecord(&list.records[i]) >= 0)
					++succeeded;
			clock_t time = clock() - start;
			printf("read succeeded %lu/%lu in %li\n", succeeded, list.num, time);
		}
		else if (strncmp(line, CMDWRITE, strlen(CMDWRITE)) == 0)
		{
			size_t succeeded = 0, skipped = 0;
			clock_t start = clock();
			for (size_t i = 0; i < list.num; i++)
			{
				result = writeRecord(list.records[i]);
				if (result >= 0)
					++succeeded;
				if (result == 1)
					++skipped;
			}
			clock_t time = clock() - start;
			sync();
			clock_t syncTime = clock() - start;
			printf("write skipped %lu succeeded %lu/%lu in %li and sync'd %li\n", skipped, succeeded, list.num, time, syncTime);
		}
		else if (strncmp(line, CMDDEL, strlen(CMDDEL)) == 0)
		{
			size_t succeeded = 0, skipped = 0;
			clock_t start = clock();
			for (size_t i = 0; i < list.num; i++)
			{
				result = deleteRecord(list.records[i].key);
				if (result >= 0)
					++succeeded;
				if (result == 1)
					++skipped;
			}
			clock_t time = clock() - start;
			sync();
			clock_t syncTime = clock() - start;
			printf("del skipped %lu succeeded %lu/%lu in %li and sync'd %li\n", skipped, succeeded, list.num, time, syncTime);
		}
		free(line);
	}

	close(dbfd);

	return 0;
}

