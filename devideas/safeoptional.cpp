  template<typename T>
  class safe_optional
  {
  private:
    std::optional<T> value;

  public:
    safe_optional() {}

    template<typename P>
    safe_optional(P && p) : value(std::forward<P>(p)) {}

#pragma warning(disable:4702)
    constexpr T & operator*() & 
    { 
      if (value) return *value; 
      ASSERT_DONT_GET_HERE;  
      return *static_cast<T*>(nullptr); 
    }
    constexpr T && operator*() && { 
      if (value) return std::move(*value); 
      ASSERT_DONT_GET_HERE;  
      return std::move(*static_cast<T*>(nullptr)); 
    }
    constexpr const T & operator*() const & { 
      if (value) return *value; 
      ASSERT_DONT_GET_HERE;  
      return *static_cast<const T*>(nullptr); 
    }
    T * operator->() {
      if (value) return value.operator->();
      ASSERT_DONT_GET_HERE; 
      return nullptr;
    }
    const T * operator->() const {
      if (value) return value.operator->(); 
      ASSERT_DONT_GET_HERE; 
      return nullptr;
    }
#pragma warning(default:4702)

    constexpr explicit operator bool() const { return value.has_value(); }

    void reset() { value.reset(); }
  };