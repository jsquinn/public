Windows development software

Windows 10 Pro

Chocolatey

Chrome/Firefox/Brave
Adblocker

.net framework?
Sysinternals
https://processhacker.sourceforge.io/

Anti-virus (Defender?
OpenShell?
Microsoft Security Essentials
Prey

Powershell
Windows Subsystem for Linux
Hyper V
Docker for windows
Putty / KiTTY, wsltty, openssh
...hosts.txt

CPU-Z, GPU-Z, HWMonitor
Speedfan, coretemp, OpenHardwareMonitor
OpenSSL?

Visual Studio 2019
Visual Studio code?

Git for Windows
Notepad++
freecommander
WinMerge, Meld
WinScp
7zip
HxD

Gimp, IrfanView, Inkscape, Paint.net
Wireshark
Vim?
Open Office

Msys/MinGW-w64
Rust
GHC
Android Studio
java
Python?

VBoxManage startvm $VM --type headless
VBoxHeadless --startvm Debian --vrdp=off
	Detach UI

Slack, Discord, Signal
http://asciiflow.com/


NirCmd
Synergy, mouse without borders

Gaming and Entertainment:

Steam
AMD/nVidia drivers
DirectX?
MPC-HC, mpv, mplayer2
MediaInfo
