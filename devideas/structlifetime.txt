


name - const copy/reference
	- compiler may copy or pass by reference
	- addresses of members should not be captured beyond scope
*name - non-const reference
	- struct lifetime unknown by compiler, programmer must know safe bounds
^ name - non-const reference with ownership
	- struct pointer will freed at end of scope, need to capture if you want 
	- parameter must be moved in
	- how to destroy? What allocator? determined by struct

** name - mutable pointer
	- ownership can be taken at will
	- alternative value can be substituted
	- struct may be destroyed if not taken (up to programmer)
	
	struct X
	{
		not_ptr: int;
		auto_ptr : !*int;
		custom_ptr : *int;
		defer custom_free(ptr);		
	}
	
	copy(x: X) -> X
	{
		tmp:X = ---;
		memcpy(tmp, x);
		tmp.auto_ptr = new(int, <<x.auto_ptr);
		tmp.custom_ptr = custom_new(int, <<x.custom_ptr);
		return tmp;
	}
	
	take(x : **X) -> *X
	{
		tmp := <<x;
		<<x = null;
		return tmp;
	}
	
	move (in : *X, out: *X) -> void
	{
		destroy(out);
		memcpy(out, in)
		memzero(in)
	}
	
	move (in: *X) -> X
	{
		tmp := <<in;
		memzero(in);
		return tmp;
	}
	
	copy #must be defined by programmer
	destroy #intrinsic (call defers, zero)
	new // malloc, memzero
	foo(x : **name)
	{
		g.y := take(name);
	}
	
	
	value semantics
		parameter
			const copy/reference
			not moved/destroyed in called function
			cannot be moved/copied/returned
			cannot take address of
			
		*parameter
			mutable reference
			data can be moved but pointer not changed (so still destroyed outside)
			
		**parameter
			mutable pointer
			ownership can be taken
			programmer must know the appropriate free method
			
		return
			memcpy (move) possibly elided
			destructed before next statement if not assigned
			
		assignment
			moved, memzero'd

		local variable
			always destructed at end of scope
			must move to avoid destruction
			
		Assigning the returned struct that was passed as a parameter? 

		captures
			[x] - const reference
			[x := ? ] 
				[ x := take(w) ] - const reference again? local variable
				[ x := copy(w) ] - local variable, destructed at scope exit

PolyMorphism?
	base class needs function pointers including a destroy function
	
	
	struct str
	{
		c: char *= null;
		len : u32 = 0;
	}
	
	struct strmem
	{
		using s: str;
		mem : char* = null;
	}
	