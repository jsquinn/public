https://github.com/u-boot/u-boot.git

https://lxr.missinglinkelectronics.com/uboot/board/sandbox/README.sandbox
sudo apt install bison flex libssl-dev
make -j8 sandbox_defconfig all NO_SDL=1

./u-boot


include/configs/at91sam9260ek.h
	doesn't seem to use SPL

#define CONFIG_SYS_SDRAM_BASE           ATMEL_BASE_CS1
#define CONFIG_SYS_SDRAM_SIZE           0x04000000
# define CONFIG_SYS_INIT_SP_ADDR \
        (ATMEL_BASE_SRAM1 + 16 * 1024 - GENERATED_GBL_DATA_SIZE)
# define CONFIG_MACH_TYPE MACH_TYPE_AT91SAM9260EK
#define CONFIG_SYS_LOAD_ADDR                    0x22000000      /* load address */

#define CONFIG_SYS_MALLOC_LEN           ROUND(3 * CONFIG_ENV_SIZE + 128*1024, 0x1000)


include/configs/at91sam9x5ek.h

#define CONFIG_SYS_SDRAM_BASE           0x20000000
#define CONFIG_SYS_SDRAM_SIZE           0x08000000      /* 128 megs */
#define CONFIG_SYS_INIT_SP_ADDR \
        (CONFIG_SYS_SDRAM_BASE + 16 * 1024 - GENERATED_GBL_DATA_SIZE)
#define CONFIG_SYS_LOAD_ADDR            0x22000000      /* load address */
#define CONFIG_SYS_MEMTEST_START        CONFIG_SYS_SDRAM_BASE
#define CONFIG_SYS_MEMTEST_END          0x26e00000
#define CONFIG_SYS_MALLOC_LEN           (512 * 1024 + 0x1000)
#define CONFIG_SPL_MAX_SIZE             0x6000
#define CONFIG_SPL_STACK                0x308000
#define CONFIG_SPL_BSS_START_ADDR       0x20000000
#define CONFIG_SPL_BSS_MAX_SIZE         0x80000
#define CONFIG_SYS_SPL_MALLOC_START     0x20080000
#define CONFIG_SYS_SPL_MALLOC_SIZE      0x80000
#define CONFIG_SYS_MONITOR_LEN          (512 << 10)

- CONFIG_SYS_MALLOC_LEN:
                Size of DRAM reserved for malloc() use.

- CONFIG_SYS_MALLOC_F_LEN
                Size of the malloc() pool for use before relocation. If
                this is defined, then a very simple malloc() implementation
                will become available before relocation. The address is just
                below the global data, and the stack is moved down to make
                space.

- CONFIG_BOARD_SIZE_LIMIT:
                Maximum size of the U-Boot image. When defined, the
                build system checks that the actual size does not
                exceed it.

- CONFIG_SYS_INIT_RAM_ADDR:
                Start address of memory area that can be used for
                initial data and stack; please note that this must be
                writable memory that is working WITHOUT special
                initialization, i. e. you CANNOT use normal RAM which
                will become available only after programming the
                memory controller and running certain initialization
                sequences.

                U-Boot uses the following memory types:
                - MPC8xx: IMMR (internal memory of the CPU)

- CONFIG_SYS_GBL_DATA_OFFSET:

                Offset of the initial data structure in the memory
                area defined by CONFIG_SYS_INIT_RAM_ADDR. Usually
                CONFIG_SYS_GBL_DATA_OFFSET is chosen such that the initial
                data is located at the end of the available space
                (sometimes written as (CONFIG_SYS_INIT_RAM_SIZE -
                GENERATED_GBL_DATA_SIZE), and the initial stack is just
                below that area (growing from (CONFIG_SYS_INIT_RAM_ADDR +
                CONFIG_SYS_GBL_DATA_OFFSET) downward.



                CONFIG_SPL_PAD_TO
                Image offset to which the SPL should be padded before appending
                the SPL payload. By default, this is defined as
                CONFIG_SPL_MAX_SIZE, or 0 if CONFIG_SPL_MAX_SIZE is undefined.
                CONFIG_SPL_PAD_TO must be either 0, meaning to append the SPL
                payload without any padding, or >= CONFIG_SPL_MAX_SIZE.

	
		OBJCOPYFLAGS_u-boot-with-spl.bin = -I binary -O binary \
						   --pad-to=$(CONFIG_SPL_PAD_TO)
		u-boot-with-spl.bin: $(SPL_IMAGE) $(SPL_PAYLOAD) FORCE
			$(call if_changed,pad_cat)

                CONFIG_SPL_TARGET
                Final target image containing SPL and payload.  Some SPLs
                use an arch-specific makefile fragment instead, for
                example if more than one image needs to be produced.


CONFIG_SYS_INIT_SP_ADDR
		location in SRAM of initial stack
		in arch/arm/cpu/arm926ejs/mxs/start.S
		in include/configs/at91sam9260ek.h this is near the top of SRAM
		with room left for GENERATED_GBL_DATA_SIZE

ATMEL AT91SAM9260
 ARM926EJ-S
	ARM 9 Core
	ARMv5TE instructinos
	ARM Jazelle for Java byte code processing
~200MHz
8KB SRAM
32KB ROM

AT91SAM9260 EK


Questions:

What is SPL?
	Secondary Program loader
	The U-boot logic to initialise system before running uboot properly
	
	call stack at prompt shows main_loop called from board_init_r
		#15 0x0000555555596b07 in main_loop () at common/main.c:63
		#16 0x00005555555999ca in run_main_loop () at common/board_r.c:638
		#17 0x0000555555599c5f in initcall_run_list (init_sequence=0x55555593af80 <init_sequence_r>) at include/initcall.h:38
		#18 board_init_r (new_gd=<optimized out>, dest_addr=<optimized out>) at common/board_r.c:866
		#19 0x000055555557975c in main (argc=1, argv=0x7fffffffdff8) at arch/sandbox/cpu/start.c:362


Why relocate at all?
	from memory mapped storage? in SRAM...
	see Initial Stack, Global Data - uboot historically run from ROM so no write to memory ability
	but why doesn't at91bootstrap just put in correct place originally?
		at91bootstrap can't calculate ideal offsets for heap/stack etc to cram uboot at very top of memory

Where is the relocation code?

	arch/arm/lib/crt0.S
		relocate_code  (gd->relocaddr) with gd->reloc_off set as the return address
	arch/arm/lib/relocate.S relocate_code(
		__image_copy_start, __image_copy_end - bytes copied
		__rel_dyn_start, __rel_dyn_end - offsets adjusted (including bss)

are global variables in bss iniialised after relocation?
	yes but always to zero
	arch/arm/lib/crt0.S SPL_CLEAR_BSS

Where are the following before relocation?
	Code
	global_data
	Stack
		arch/arm/lib/crt0.S	
		ldr     r0, [r9, #GD_START_ADDR_SP]     /* sp = gd->start_addr_sp */

	Heap
	Global/statics - unavailable maybe located in readonly area...

	see arch/arm/cpu/armv8/u-boot.lds for code, global layout

Heap size
	128KB
	include/configs/at91sam9260ek.h
	#define CONFIG_SYS_MALLOC_LEN		ROUND(3 * CONFIG_ENV_SIZE + 128*1024, 0x1000)
        TOTAL_MALLOC_LEN = CONFIG_SYS_MALLOC_LEN

	common/board_r.c initr_malloc()
	malloc_start = gd->relocaddr - TOTAL_MALLOC_LEN;
	mem_malloc_init((ulong)map_sysmem(malloc_start, TOTAL_MALLOC_LEN),
                        TOTAL_MALLOC_LEN);
        return 0;

Where are the above after relocation?
	see common/board_f.c
	static const init_fnc_t init_sequence_f[] = { ... }
		setup_dest_addr gd->relocaddr = gd->ram_top
		reserve_uboot, gd->relocaddr -= gd->mon_len
				gd->start_addr_sp = gd->relocaddr
		reserve_malloc, gd->start_addr_sp -= TOTAL_MALLOC_LEN
			TOTAL_MALLOC_LEN = CONFIG_SYS_MALLOC_LEN + env size
		reserve_board, gd->start_addr_sp -= sizeof(bd_t) (gd->bd)
		reserve_global_data, gd->start_addr_sp -= sizeof(gd_t) (gd->new_gd)
		reserve_stacks, arch_reserve_stacks
		setup_reloc,

	stack:
		# ifdef CONFIG_SPL_BUILD
                        /* Use a DRAM stack for the rest of SPL, if requested */
                        bl      spl_relocate_stack_gd
                        cmp     r0, #0
                        movne   sp, r0
                        movne   r9, r0
                # endif

                #ifdef CONFIG_SPL_STACK_R - CONFIG_SPL_STACK_R_ADDR
                #else spl_relocate_stack_gd returns 0, r9 = gd->relocaddr



Can any other areas of SDRAM be used?
	ATAGS... bdinfo boot_params = 0x20000100


Debug build

		#ifndef DEBUG
		#define DEBUG
		#endif

		in include/common.h
		breaks zlib compatibility because uboot putc is not consistent with c standard
		include/log.h +100
		#if CONFIG_IS_ENABLED(LOG)
		#ifdef LOG_DEBUG
		#define _LOG_DEBUG      1
		#else
		#define _LOG_DEBUG      0
		#endif


CONFIG_SPL_RELOC_TEXT_BASE
CONFIG_SPL_BSS_START_ADDR
CONFIG_SPL_BSS_MAX_SIZE
CONFIG_SPL_STACK - not used by atmel/sandbox
CONFIG_SPL_STACK_R_ADDR - not used by atmel/sandbox
CONFIG_SPL_RELOC_STACK - not used by atmel/sandbox
CONFIG_SYS_SPL_MALLOC_START
CONFIG_SYS_SPL_MALLOC_SIZE
initr_reloc,
initr_reloc_global_data,
initr_barrier,
initr_malloc,
log_init,

