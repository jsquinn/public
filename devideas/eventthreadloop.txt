Initialise
    |
    |
    v
Blocking wait for input/timeout <-+
    |                             |
    |                             |
    v                             |
Process input/timeout    Calculate next timeout (if needed)
    |                             ^
    |                             |
    v                             |
Finished? ----Not Yet--> Determine input channels (if needed)
    |
   Yep
    v
Shutdown
