Why have a controller

- Can't connect every thing to a Server PC (or USB) due to cost of cabling and lack of IO
- PC is not stable enough to host access/alarms logic (out of our control)
- Exposing Access Control IO or corporate network to unsecure area is not desirable (hardened gateway)
- scales?

So the controller:
- securely passes information between local IO/devices and server
- stores information destined for devices/server until they are available
- performs logic
	- caching state/configuration required for this
- routes messages for other controllers
	- managing connections
- allows for diagnostics for its own complicated system
- performs download/upgrade logic

How could it do these better:
	- Store more data destined for the server
	- Send data destined to the server faster
	- use stronger security when passing to the server
	- more future proofing in protocol/internal system
		- actual databases
		- limits on enums more than 255
	- communicate to more/different devices
		- more hbus, wireless, cctv, image capture, audio, analog, text
		- ethernet? radio, serial, usb, firewire? cellular
	- perform logic faster
	- more inter-controller connections
	- more reliable inter-controller connections
	- better diagnostics
	- better reliability in infrastructure
	- better development tools (VS)
	
What also could it be?
	secure boot 
	Text/analog IO (ADC,DAC,PWM)/image/audio/cctv/email/text
	HDMI site diagnostic host?
	Wireless host
	USB host for extended peripherals
	Bluetooth control?
	Protected environment for third party software (java VM?)
	
What for?
  building management
  factory automation
  system management
  
What could replace it?
	- access control between phone and server (with NFC challenge chips per door)
		- phone could be online or offline
	- ethernet/wifi based edge devices with server centric control/built in access logic
	- pc based controller with USB peripherals (low cost for logic/storage)

Hardware/Compatilbility:
  DC power 13.6v
  Ethernet (dual on high spec)
  Cpu/Ram/Storage
  debug (USB?)
  serial->dialler?
  inservice relay
  RS485
    - GBus?
    - HBus?
    - higher performance unencrypted?
    - WBus
  bacnet?
  Inputs/Outputs
  Cardax IV?
  SAM fip140 approved
  Power, status, LEDs
  Configuration dipswitches
  Tamper detection
  Low Power detection

Characteristics:
  Form factor?
  Component and software Lifespan?
  upgradability, downgradability & maintenance
  thermal & power usage
  customisations
  installation (ip allocation)
  Macros?
  Security (boot, peripherals, storage, OpenSSL FIP201)
  Latency with peripheral devices
  CPU Performance
  Memory
  persistent storage
Local cache
  Pertinent configuration
  Credential Database for access
  Software Downloads
  
Improvements
  JTAG/Debug headers
  Specific IP via USB/Serial?
  
What does our controller do?

Communication (provides hardware, low latency embedded logic)
  - bus device protocols/layers/units/drivers  (HBus, GBus, Aperio, OSDP, Schlage, Sensor)
    OTIS, backnet, moduteq, modbus, Morpho, IP Alarm transmitter
  - dialler
  - ethernet TCP/IP, TLS, (dual on high spec)
  - ppp modem via serial
  - HLI serial
  - plugin module
  - local inputs/outputs/readers
  - routing

Device Logic
  - device software downloads
  - [virtual] Outputs (pulse, timing, diary)
  - input states
  - reader UI logic (LED, sound, IDT text, keypad)
  - Fence Control
  - RAT UI/service logic
  - T2x service logic
  - camera
  - taut wire group wire deflection engine
  - Door control (+interlock)
  - HLI Elevator control (+Kone)
  - others?
  
Application Logic 
  - Alarm management
    - Alarm Zone states
    - Arming/exit delay/entry delay
    - Alarm tracking 
    - Action plans
    - Contact Id
  - Access control Logic (who, where, when)
    - personalised actions
    - competencies
    - first card unlock
    - disarm on entry
    - lock down
    - Anti-passback, zone counting
    - visitors
    - etc
  - Card Decoding
    - PIV transactions 
    - BLE transactions
  - Customisations
  - Logic blocks
  - Routing between parts/controller/server
  
Network and communication Logic
  - Reliable routing (recovery from comms loss)
  - TLS Connection management and rollover (send and receive)
  - Secondary Ethernet fallback
  - Bertbus/Transfer messaging
  - Heartbeats and Offline monitoring
  
Compute & Crypto
  - HBus ECC/AES
  - PIV verification RSA/ECC
  - BLE/FIDO sha256/ECC
  - mifare site key
  
Caching & Persistence
  - Access Credentials (cards, usercodes, PIV hashs, BLE credentials)
  - Access Groups (clubs, diaries)
  - Associated Cardholder data (Card trace, CCC number, competencies, cardholder id?...)
  - Schedules/Diaries (access Mode, Alarm state, Cardholder Access, outputs, fence modes)
  - Overrides
  - TLS Certificates
  - HBus keys
  - names (cardholder, club, input, output, fence, alarmzone, event) for to display on RAT/IDT/T2x
  - RAT strings
  - part configuration (includes competency strings)
  - device software
  - event and alarm delivery
  - Database lookup and storage logic
  
Controller Maintenance & Diagnostics
  - controller downloads (Controller software, Customisations, HBus Device software)
  - USB upgrades (Controller software, Customisations, HBus Device software)
  - dipswitches (reset cfg/software)
  - Watchdog
  - Controller overrides (web page, 
  - temperature monitoring
  - power monitoring
  - tamper detection
  - web page
  - low level memory monitoring
  - error/debug logging
  - Secure debug (USB dongle)

System
  - Operating system
  - device drivers
    - network
    - storage
    - hbus/serial
    - usb debug
  - initialisation process
  - services 
    - debug telnet, ftp, active sync
  - locked down for security
  
  





	