https://unqlite.org/
https://docs.rs/unqlite/1.5.0/unqlite/


# Relational sql

Relational sql databases provide a general solution to storing abitary data with a compromise of the following properties:

read/write performance
reasonable storage space usage
extensibility/restructurability

with the following guarantees:

transactability
corruption protection/recovery
security/encryption
good performance for a wide range for data structures.

developers can guide performance tradeoffs with
column data types
primary/secondary keys
additional indicies

But by an large the database engines attempt quite admirable performance while maintaining other properties.

However the developer does not determine the performance charactistics of the database - the engine does.
And the engine designers can change this behaviour over time.
The engine is designed to be a generic solution for any data representation so cannot be optimised specifically for your design.

At some level of scale a sql database will encounter underdesireable performance due to the inheritant trade offs of being a generic solution.


# What would a simple custom solution be?

multiple 'tables' each with the following:
meta data describing state of the table
	- format version (for upgrading/backward compatibility)
	- indices to maintain
	- transaction status? added/removed records
a 'container' file with fixed size records - some may be null/empty
a 'addtransaction' file with records to be added.
a 'updatetransaction' file with records to be updated.
a 'removetransaction' file with record locations to be removed.
a 'strings' file for variable strings (using a heap memory style algorithm)
	- a string will only be referenced by a single record with short string optimisations
a 'addstrings' file with strings to add
a 'removestrings' files with locations to remove
multiple indices files maintaining fixed records with sorted keys and record locations.

Code would have to perform a lookup based on an existing index ( > and < range search).
Code would then loop through index results and read records to filter further.
Cross referencing would have to be performed manually using results from one search to search another table.

row modification would take place in a transaction:
- change transaction status to all affected tables to 'started'
- write added records
- write removed record location
- sync
- change transaction status to all affected tables to 'applying'
- remove (nullify) records
- remove strings
- add strings
- update records
- remove updated strings
- add records
- sync
- change transaction status to all affected tables to 'indexing'
- update indices
- sync
- change transaction status to all affected tables to 'complete'

on opening 'database' we can recovery from corruption:
- started - abandon transaction on these tables, delete added/edited/removed records and now 'complete'
- applying - reapply on these tables and continue transaction
- indexing - reindex on these table and continue transaction
- complete - all good
This is dependent 'sync' saving all data across all files before returning
This is dependent on all files to have either the original data or only some of the changes attempted by software
if pages in the file are randomly corrupted on power loss while writing then nothing can be saved unless we create
complete new files each time.

## Saving space

Variable large binary data could be stored in a separate pool and referred by SHA3 hash.
But this may take more space than the data usually would?

Most strings are generally small but you may want to support quite large exceptions...
That suggests a string heap file is required, but maybe that data could be stored outside in the records and
restricted from being a key (look up person by name seems a reasonable request though...).

## Encryption

Records, indexes could be stored in blocks that are encrypted.
Would you validate all data on startup?

Would you just encrypt sensitive data in the records and protect access using operating system permissions?

## Restructuring

If the record shape has to change then a new table must be created from the old from scratch
reading old records, adding new ones and building new indexies.
Maybe the code can just create a new table with the new record structure, start a transaction,
record all the additions and complete the transaction. Then rename the old and the new before deleting the old.

## Quadruple store:

For recursive relational data (with a single table) this could be store in records of 4 ints
 - source id [pk]
 - dest id [pk]
 - relationship [pk]
 - relationship number

indicies kept on pk columns and any two pk

## Additional corruption or bad upgrade recovery:

All transactions could be stored as json in a git repo
A file for each record : table/key.json
sensitive record values (pins, names etc) could be encrypted
Full history available for diagnostics.
All databases could be rebuilt from scratch using the git repo records.

## Remote DB performance

The logic interacting with persistent data would always perform on the same machine as the data.
If a central server using this DB is running on another machine then it will need its own protocol
to request the queries to perform and the local DB service perform the lookups/edits needed
and format the desired data.

This is in contrast to a sql db where the central server could send a sql queries to a remove sql db service
and interact with streamed data (slowly).

## Testability

Service level tests on the 'local persistence service' could be done readily.
The DB logic only needs a directory and all installation is controlled by the code.

# Conclusion:

I'm sure if this fit the bill it could be faster, easier to diagnose and maintain.
You have complete understand of the algorithms, data structures even the machine instructions being performed.
But it could be a lot of work, and a bug could be incredibly painful and embarrassing
And eventually (maybe rather soon) the limited query capabilities create a real performance issue.



# Other

https://codeburst.io/doing-without-databases-in-the-21st-century-6e25cf495373

https://www.codeproject.com/Articles/308199/A-Lightweight-Indexed-File-for-Storing-Persistent

