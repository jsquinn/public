What is a language?
What do I want in a language?
What language is the best fit for my team to deliver quality software quickly?

a programming Language is not just its syntax and semantics.

A language is full of compromises
	Safety
	Convinence
	Performance
	Expressiveness
	Simplicity
	Hardware support
	Platform independence
	Control
	Compile time/resources
	Language Compatbility (C)

It can't be judged on just these criteria
There are also attributes out side of the language formal definition
That are possibly more important in its selection
	Toolchain and Platform support
	Formalisation, maturity, stability
	Community support and co-ordination, future direction
	Learning resources, skill availability for employment
	Standary library, library ecosystem

Long term Rust seems my preferred solution for many problems.
 - language stability, industry knowledge seem speed bumps at the moment.
 - Design complexity, slow compilation time seem permanent attributes.


Although bash, Powershell, html/js will always have their niche.
Windows GUI without C# feasible? GTK/QT on linux? Cocoa on Mac?
