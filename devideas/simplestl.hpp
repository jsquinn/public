// simplestl.hpp

#include <cstdint>
#include <memory>
#include <string>
#include <vector>
#include <map>
#include <tuple>
#include <string_view>

using namespace std::literals;

using u8 = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;
using i8 = int8_t;
using i16 = int16_t;
using i32 = int32_t;
using i64 = int64_t;

using strbuf = std::string;
using str = std::string_view;

template<typename T>
using ref = const T&;

template<typename T>
using mutref = T&;

template<typename T>
using own = T&&;

template<typename T>
using copy = T;


template<typename T>
using ptr = std::unique_ptr<T>;

template<typename... Args>
using make_ptr = std::make_unique<Args...>;

template<typename T, typename D>
auto make_ptrdel(T * p, D d)
{
  return std::unique_ptr<T, D>(t, d);
}

using make_fptr = make_ptrdel<FILE, decltype(&fclose)>;

template<typename T, typename U>
auto & ptr_cast(U & u)
{
  return static_cast<T&>(*u.get());
}

template<typename T, typename U>
const auto & ptr_cast(const U & u)
{
  return static_cast<const T&>(*u.get());
}

template<typename T>
class box
{
public:
  box() : t(mkptr<T>()) {}
  
  template<typename... Args>
  box(Args... args) : t(mkptr<T>(std::forward<Args>(args)...)) {}
  
  box(const &box<T>) = delete;
  box(box<T> &&) = default;
  
  T& operator*() { return *t; }
  const T& operator*() const { return *t; }
  
  T& operator->() { return *t; }
  const T& operator->() const { return *t; }
  
private:
  ptr<T> t;
}

template<typename T>
using vec = std::vector<T>;

template<typename T>
using map = std::map<T>;

template<typename T>
using opt = std::optional<T>;

template<typename... T>
using tup = std::tuple<T...>;

using tup1 = std::get<1>;
using tup2 = std::get<2>;
using tup3 = std::get<3>;
using tup4 = std::get<4>;
using tup5 = std::get<5>;
using tup6 = std::get<6>;
using tup7 = std::get<7>;
using tup8 = std::get<8>;
using tup9 = std::get<9>;

template<typename T, typename V>
bool contains(T && t, V && v)
{
  return std::find(std::begin(t), std::end(t), std::forward<V>(v)) == std::end(t);
}

template<typename T, typename V>
auto find(T && t, V && v)
{
  return std::find(std::begin(t), std::end(t), std::forward<V>(v));
}

template<typename T, typename V>
auto find_if(T && t, V && v)
{
  return std::find_if(std::begin(t), std::end(t), std::forward<V>(v));
}

template<typename T, typename L, typename... P>
void foreach(T && t, L && l, P && .... p)
{
  for (auto && v : t)
  {
    l(std::forward<P>(p>...);
  }
}

template<typename T, typename L, typename... P>
void whileeach(T && t, L && l, P && .... p)
{
  for (auto && v : t)
  {
    if (!l(std::forward<P>(p>...))
      return false;
  }
  
  return true;
}

#define Lx(code) [](auto x) { return code; }
#define Lxy(code) [](auto x, auto y) { return code; }
#define Lxyz(code) [](auto x, auto y, auto z) { return code; }

template<typename TC, typename F>
auto vecmap(const TC src, F && func);

template<typename T, typename Container<T>, typename F>
auto vecmap(const Container<T> src, F && func)
{
  vec<T> result;
  result.reserve(std::distance(std::begin(src), std::end(src)));
  std::transform(std::begin(src), std::end(src), std::inserter(result), std::forward<F>(func));
  return result;
}

template<typename T, size_t n, typename F>
auto vecmap(const T[n] src, F && func)
{
  vec<T> result;
  result.reserve(std::distance(std::begin(src), std::end(src)));
  std::transform(std::begin(src), std::end(src), std::inserter(result), std::forward<F>(func));
  return result;
}


template<typename Container<T>, typename TC, typename F>
auto cmap(const TC src, F && func);

template<typename Container<T>, typename T, typename Container<T>, typename F>
auto cmap(const Container<T> src, F && func)
{
  Container<T> result;
  result.reserve(std::distance(std::begin(src), std::end(src)));
  std::transform(std::begin(src), std::end(src), std::inserter(result), std::forward<F>(func));
  return result;
}

template<typename Container<T>, typename T, size_t n, typename F>
auto cmap(const T[n] src, F && func)
{
  Container<T> result;
  result.reserve(std::distance(std::begin(src), std::end(src)));
  std::transform(std::begin(src), std::end(src), std::inserter(result), std::forward<F>(func));
  return result;
}

/*
  vector<int> nums = {1,2,3};
  auto strs = cmap(nums, Lx(std::to_string(x)));
*/

template<typename T>
auto cleanup(T && t)
{
  struct D
  {
  public:
    D(T && t) : m(std::forward<T>(t)) {}
    ~D() { std::invoke(m); }
    D(const D &) = delete;
    D(D &&) = default;
  private:
    T m;
  };

  return D{ std::forward<T>(t) };
}

class cmp
{
public:
  template<typename L, typename R>
  cmp(L && l, R && r)
  {
    if (l < r)
      m_value = -1;
    else if (l > r)
      m_value = 1;
  }
  
  template<typename L, typename R>
  cmp & c(L && l, R && r)
  {
    if (m_value != 0)
       return *this;
    
    if (l < r)
      m_value = -1;
    else if (l > r)
      m_value = 1;
    return *this;
  }
  
  bool lt const() { return m_value < 0; }
  bool gt const() { return m_value > 0; }
  bool eq const() { return m_value == 0; }
  bool ne const() { return m_value != 0; }
  
private:
  int m_value = 0;
}



template Destructor<typename ValueType, typename DeleteFunc, ValueType NullValue>
struct Destructor { 
	ValueType x; 
	Destructor() : x(NullValue) {} 
	Destructor(ValueType _x) : x(_x) {} 
	~Destructor() { if (x != NullValue) DeleteFunc(x); } 
	operator ValueType&() { return x; } 
};
// DESTRUCTOR 

#define DESTRUCTOR(VALUE_TYPE, DELETE_CALL, NULL_VALUE) \
	struct VALUE_TYPE##Destructor { \
		VALUE_TYPE x; \
		CLASS_NAME() : x(NULL_VALUE) {} \
		CLASS_NAME(VALUE_TYPE _x) : x(_x) {} \
		~CLASS_NAME() { if (x != NULL_VALUE) DELETE_CALL(x); } \
		operator VALUE_TYPE&() { return x; } \
	};
	

class cstr
{
private:
	const char * m_begin;
	const char * m_end;
public:
	cstr(const char *str) : m_begin(str), m_end(str == nullptr ? str : str + strlen(str)) {}
	cstr(const strbuf &str) : m_begin(str.begin()), m_end(std.end()) {}
	
	const char & at(size_t pos) { if (pos < 0 || pos >= size()) throw std::out_of_range(); return m_begin[pos]; }
	const char & front(size_t pos) { return at[0]; }
	const char & back(size_t pos) { return at[size()-1]; }
	size_t size() const { return std::distance(m_begin, m_end); }
	bool empty() const { return m_begin == m_end; }
	const char * begin() const { return m_begin; }
	const char * cbegin() const { return m_begin; }
	const char * end() const { return m_end+1; }
	const char * cend() const { return m_end+1; }
	
	// does this work with std::advance/std::distance?
	const char * rbegin() const { return m_end; }
	const char * crbegin() const { return m_end; }
	const char * rend() const { return m_begin-1; }
	const char * crend() const { return m_begin-1; }
}