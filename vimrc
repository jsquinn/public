set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'wsdjeg/vim-fetch'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line



set relativenumber
set number
set tabstop=4
set shiftwidth=4
set autoindent
set listchars=tab:\`\ ,trail:_,extends:>,precedes:<
set nowrap
set autoindent
set smarttab
set incsearch
set ruler
set backspace=indent,eol,start
set wildmenu
set showcmd
set t_Co=256
set list
colo elflord
set path+=**
set display=uhex

if &diff
	colo slate
endif

if executable('ag') 
	" Note we extract the column as well as the file and line number
	 set grepprg=ag\ --nogroup\ --nocolor\ --column
	 set grepformat=%f:%l:%c%m
endif

nmap <silent> <F4> :cnext<CR>
nmap <silent> <F3> :cprev<CR>

" bind K to grep word under cursor
nnoremap K :grep "\b<C-R><C-W>\b"<CR>:cw<CR>
nnoremap CTRL-K :g/"\b<C-R><C-W>\b"/p<CR>
