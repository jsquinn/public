#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include <cassert>

struct abcdef 
{
	int xyz;
	std::vector<std::string> strv;
};

using tt = std::tuple<int, std::vector<std::string>>;
struct t2 { int i = {}; std::vector<std::string> j = {}; };

int g = 123;

void setg()
{
	g = 234;
}
int main(int argc, char *argv[])
{
	std::cout << "Hello World\n";
	setg();
	std::cout << g << std::endl;

	//assert(false);

//#	abcdef a;
//#	a.strv.emplace_back("a1");
//#	a.strv.emplace_back("b2");
//#	std::cout << a.strv.size();
//#
//#	t2 x{1, {"bc"}};
//#	t2 y{1};
//#
//#	tt z{5, {"hrts", "htrbrt"}};

//	std::cout << "Int size = " << sizeof(int) << std::endl;
//	std::cout << "Long size = " << sizeof(long) << std::endl;
//	std::cout << "Long Long size = " << sizeof(long long) << std::endl;
//	std::cout << "Size_t size = " << sizeof(size_t) << std::endl;
//	std::cout << "Void* size = " << sizeof(void*) << std::endl;
	return 0;
}

