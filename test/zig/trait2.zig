const warn = @import("std").debug.warn;

// from C++ background I expected casting to and from void* to work
// but no https://github.com/ziglang/zig/issues/960
// so gotta use @ptrToInt/@intToPtr to store arbitary pointers?
const traitid = usize;

const Trait = struct {
    foo: fn (t: traitid, i: i32) void,
    foo2: fn (t: traitid, i: i32, j: i32) void,
};

const Impl = struct {
    fn foo(t: Impl, i: i32) void {
        warn("impl.foo {} {}\n", .{ t.magic, i });
    }
    fn foo2(t: Impl, i: i32, j: i32) void {
        warn("impl.foo2 {} {} {}\n", .{ t.magic, i, j });
    }
    magic: i32 = 0x1234567,
};

const TraitFatPtr = struct {
    t: Trait,
    p: traitid,
    // without structure reification the following boiler plate is required per trait
    // I think you would also need to reify functions pass the argument types as a structure (e.g. std.format)
    fn call_foo(self: TraitFatPtr, i: i32) void {
        return self.t.foo(self.p, i);
    }
};

fn makeComptimeStruct(comptime I: type) type {
    // can only create a function in zig with a computed type a via struct :/
    // https://github.com/ziglang/zig/issues/229
    return struct {
        fn foo(t: traitid, i: i32) void {
            return I.foo(@intToPtr(*I, t).*, i);
        }
        fn foo2(t: traitid, i: i32, j: i32) void {
            return I.foo2(@intToPtr(*I, t).*, i, j);
        }
    };
}

fn makeTraitFatPointer(i: var) TraitFatPtr {
    const I = @TypeOf(i.*);
    const SI = makeComptimeStruct(I);

    return TraitFatPtr{
        .t = Trait{
            .foo = SI.foo,
            .foo2 = SI.foo2,
        },
        .p = @ptrToInt(i),
    };
}

pub fn main() void {
    warn("Hello, world!\n", .{});

    var i = Impl{};
    Impl.foo(i, 123);
    i.foo2(321, 321);
    i.magic += 3;
    var tfp = makeTraitFatPointer(&i);
    //    tfp.call_foo(321);
    tfp.call_foo(123);
}
