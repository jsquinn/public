const std = @import("std");
const warn = @import("std").debug.warn;

const Inner1 = struct {
    fn destroy(x: @This()) void {
        warn("Inner1.destroy\n", .{});
    }
};
const Inner2 = struct {
    fn destroynot(x: @This()) void {
        warn("Inner2.destroy\n", .{});
    }
};
const Inner3 = struct {
    fn destroy(x: @This()) void {
        warn("Inner3.destroy\n", .{});
    }
};

const Outer = struct {
    i: i32,
    j: i64,
    in1: Inner1,
    in2: Inner2,
    in3: Inner3,

    fn destroy(x: @This()) void {
        warn("Outer.destroy\n", .{});
    }
    fn nodestroy() void {}
};

pub fn union_is(u: var, name: []const u8) bool {
    const T = @TypeOf(u);
    const TagType = @TagType(T);
    const Tag = @as(TagType, u);
    const TagT = @TypeOf(Tag);

    inline for (@typeInfo(TagType).Enum.fields) |f| {
        if (std.mem.eql(u8, f.name, name)) {
            return @enumToInt(Tag) == f.value;
        }
    }
    //std.debug.warn("Tag {} {} {} {}\n", .{ u, Tag, TagT,  });
    return false;
}

fn is_struct(comptime v: var) bool {
    const Ti = @typeInfo(@TypeOf(v));
    return Ti == std.builtin.TypeId.Struct;
}

fn recursive_destroy(v: var) void {
    const Ti = @typeInfo(@TypeOf(v));
    //   comptime std.debug.assert(Ti == std.builtin.TypeId.Struct);
    comptime var i = Ti.Struct.fields.len;
    inline while (i != 0) {
        comptime i -= 1;
        //@compileLog(" i = ", i);
        //@compileLog(" Ti[i] = ", Ti.Struct.fields[i].field_type);
        //std.debug.warn("{}\n", .{Ti.Struct.fields[i].name});
        //comptime var is = is_struct(Ti.Struct.fields[i].field_type);
        comptime var is = @typeInfo(Ti.Struct.fields[i].field_type) == std.builtin.TypeId.Struct;
        //comptime var is = is_struct(@field(v, Ti.Struct.fields[i].name));
        //@compileLog(" is = ", is);
        if (comptime is) {
            recursive_destroy(@field(v, Ti.Struct.fields[i].name));
        }
    }
    const DeclType = std.builtin.TypeId.Declaration.Data;
    const FnType = std.builtin.TypeId.Declaration.Data.Fn;
    const DeclTagType = @TagType(DeclType);
    inline for (Ti.Struct.decls) |d| {
        if (comptime std.mem.eql(u8, d.name, "destroy") and @as(DeclTagType, d.data) == FnType) {
            v.destroy();
        }
    }
}

pub fn main() void {
    std.debug.warn("Hello, world!\n", .{});
    var i = Outer{ .i = 1, .j = 2, .in1 = Inner1{}, .in2 = Inner2{}, .in3 = Inner3{} };
    i.destroy();
    var tt = @as(std.builtin.TypeId, @typeInfo(Outer));

    std.debug.warn("Hello, world! {}\n", .{tt});

    inline for (@typeInfo(Outer).Struct.fields) |f| {
        if (@as(std.builtin.TypeId, @typeInfo(f.field_type)) == std.builtin.TypeId.Fn) {
            std.debug.warn("fn {} \n", .{f.name});
        } else {
            std.debug.warn("nfn {} \n", .{f.name});
        }
    }

    const DeclType = std.builtin.TypeId.Declaration.Data;
    const FnType = std.builtin.TypeId.Declaration.Data.Fn;
    const DeclTagType = @TagType(DeclType);

    std.debug.warn("decl {} {} {} \n", .{ DeclType, DeclTagType, FnType });
    inline for (@typeInfo(Outer).Struct.decls) |d| {
        if (std.mem.eql(u8, d.name, "destroy") and @as(DeclTagType, d.data) == FnType) {
            std.debug.warn("fn {} \n", .{d.name});
        } else {
            std.debug.warn("nfn {} \n", .{d.name});
        }
    }

    std.debug.warn("union is {}\n", .{union_is(@typeInfo(Outer), "Struct")});
    std.debug.warn("union is {}\n", .{@typeInfo(Outer) == std.builtin.TypeId.Struct});
    recursive_destroy(i);
}
