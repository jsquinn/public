const warn = @import("std").debug.warn;

const s = struct {
    i: i32
};

fn genType() type {
    return struct {
        i: i32 = 5,
    };
}

const dyns = genType();
pub fn main() void {
    var x = dyns{};
    warn("Hello, world! {}\n", .{x.i});
}
