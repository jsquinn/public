const warn = @import("std").debug.warn;

const Trait = struct {
    foo: fn (t: *Trait, i: i32) void,
    foo2: fn (t: *Trait, i: i32, j: i32) void,

    fn call_foo(t: *Trait, i: i32) void {
        return t.foo(t, i);
    }
};

const Impl = struct {
    fn me(t: *Trait) *Impl {
        return @fieldParentPtr(Impl, "tr", t);
    }
    fn impl_foo(t: *Trait, i: i32) void {
        warn("impl.foo {} {}\n", .{ me(t).magic, i });
    }
    fn impl_foo2(t: *Trait, i: i32, j: i32) void {
        warn("impl.foo2 {} {} {}\n", .{ me(t).magic, i, j });
    }
    tr: Trait = Trait{ .foo = impl_foo, .foo2 = impl_foo2 },
    magic: i32 = 0x1234567,
};

pub fn main() void {
    warn("Hello, world!\n", .{});
    var im = Impl{};
    var t: *Trait = &im.tr;

    t.foo2(t, 1, 2);
    t.call_foo(1);
    //var f: fn (i32) void = testfn;
    //f(432);
    //var o = Outer{};
    //f(o.in.i(&o.in));
}
