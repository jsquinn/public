const std = @import("std");

const Base = struct {
    f: fn(v: *Base) void,
    fn print(v: *Base) void {
         std.debug.warn("print\n", .{});
    }
    fn init() Base {
       return  Base {
            .f = print
        };
    }
};

const Derived = struct {
    b: Base,
    i: i32,
    fn init() Derived {
        return  Derived {
             .b = Base {
             .f = print
              },
             .i = 45
           };
     }
    fn me(v: *Base) *Derived {
         return @fieldParentPtr(Derived, "b", v);
    }
    fn print(v: *Base) void {
        std.debug.warn("print2 {}\n", .{ me(v).i });
    }
};

pub fn main() void {
    std.debug.warn("Hello, world!\n", .{});

    var s= Base.init();
    s.f(&s);

    var d = Derived.init();
    d.b.f(&d.b);
}

