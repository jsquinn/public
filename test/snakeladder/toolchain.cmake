# the name of the target operating system
set(CMAKE_SYSTEM_NAME Linux)

# which compilers to use for C and C++
set(CMAKE_CXX_COMPILER g++-10)
set(CMAKE_CXX_FLAGS -std=c++20)

