#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include <cassert>

using RollSeq = std::basic_string<int>;

static constexpr int MIN_ROLL = 1;
static constexpr int MAX_ROLL = 6;
static constexpr int WIN_LOC = 101;

struct Path
{
	int loc = 0;
	RollSeq rolls;

	bool operator<(const Path & path) const
	{
		if (path.loc >= WIN_LOC)
		{
			if (loc >= WIN_LOC)
			{
				if (rolls.size() == path.rolls.size())
					return loc < path.loc;
				return rolls.size() > path.rolls.size();
			}

			return true;
		}
		else
		{
			
			if (loc >= WIN_LOC)
			{
				return false;
			}

			return loc < path.loc;
		}
	}
};

class Results
{
private:
	std::vector<Path> best;

public:
	Results(int max) : best(max+1) {}

	void Add(Path && path)
	{
		static auto gt = [](const Path & l, const Path &r) { return r < l; };

		std::pop_heap(best.begin(), best.end(), gt);
		best.back() = std::move(path);
		std::push_heap(best.begin(), best.end(), gt);
	}

	std::vector<Path> GetBest()
	{
		int n = 0;
		std::vector<Path> ret;

		std::make_heap(best.begin(), best.end());
		do {
			std::pop_heap(best.begin(), best.begin() + (best.size() - n));
			if (best.back().loc == 0)
				break;
			ret.emplace_back(best[best.size() - n - 1]);
			n++;
		} while (n + 1 < best.size());

		return ret;
	}
};


int step(int loc, int die)
{
	loc += die;

	if (loc >= WIN_LOC)
		return loc;
	
	switch (loc)
	{
		// snakes
	case 16: return 6;
	case 47: return 26;
	case 49: return 11;
	case 56: return 53;
	case 62: return 19;
	case 64: return 60;
	case 87: return 24;
	case 93: return 73;
	case 95: return 75;
	case 98: return 78;

	// ladders
	case 1: return 38;
	case 4: return 14;
	case 9: return 31;
	case 21: return 42;
	case 28: return 84;
	case 36: return 44;
//	case 39: return 99; // fake
	case 51: return 67;
	case 80: return 100;

	default:
		return loc;
	}
}

void CalculateRolls(const Path & base, int depth, Results & results)
{
	if (depth == 0 || base.loc >= WIN_LOC)
		return;

	for (int i = MIN_ROLL; i <= MAX_ROLL; ++i)
	{
		auto loc = step(base.loc, i);
		auto path = Path(loc, RollSeq(base.rolls) + i);
		if (loc >= WIN_LOC || depth == 1)
		{
			results.Add(std::move(path));
		}
		else
		{
			CalculateRolls(path, depth-1, results);
		}
	}
}


int main(int argc, char *argv[])
{
	std::cout << "Hello World\n";
	
	int nresults = 10;
	if (argc >= 3)
		nresults = std::stoi(argv[2]);
	int depth = 5;
	if (argc >= 2)
		depth = std::stoi(argv[1]);

	Results results {nresults};

	CalculateRolls({}, depth, results);

	std::cout << "Best:" << std::endl;

	auto best = results.GetBest();

	for (int i = 0; i < best.size()-1; ++i)
	{
		std::cout << i+1 << " : ";
		for (int j = 0; j < best[i].rolls.size(); ++j)
			std::cout << best[i].rolls[j] << ", ";
		if (best[i].loc >= WIN_LOC)
			std::cout << "=> WIN in " << best[i].rolls.size() << std::endl;
		else
			std::cout << "=> " << best[i].loc << std::endl;
		int loc = 0;
		std::cout << "  ";
		for (int j = 0; j < best[i].rolls.size(); ++j)
		{
			int xloc = loc + best[i].rolls[j];
			loc = step(loc, best[i].rolls[j]);
			if (xloc != loc)
				std::cout << xloc << "->" << loc << ", ";
			else
				std::cout << loc  << ", ";
		}
		std::cout << std::endl;
	}

	return 0;
}

